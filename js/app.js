
// Vous devez récuperer les valeurs des champs du formulaire, en faire un joli objet et afficher le résultat dans la balise div#render.



//------------------------------------------>   jolie objet : faire des input value des objet : be_objet 
var be_objet = new Object() 
$("input").keypress(function (e) { //------->  keypress = touche like button ! functione(e) = choisir la touche  									

    if (e.which == 13) //------------------->  recupère la touche 'ENTRER' s'appelle 13
    {
        //---------------------------------->  recuperer les valeur input
        var prenom = $("#first_name").val(); 
        console.log(prenom);
        var nom = $("#last_name").val();
        console.log(nom);
        var city = $("#city").val();
        console.log(city);

        //---------------------------------->  input value = nouvelle objet 
        be_objet.Prenom = prenom;
        be_objet.Nom = nom;
        be_objet.City = city;
        console.log(be_objet);

        //---------------------------------->  Ajouter tout les objet/value ensemble
        var phrase = be_objet["Prenom"] + " " + be_objet["Nom"] + " de " + be_objet["City"];
        console.log(phrase);

        //----------------------------------> remplacer le texte de div#render par le span#username
          $("#username").replaceWith(phrase);    
    }

})